Calcule les dates de début/fin de sprints d'un projet à partir de :
- la date de début du projet (doit être un lundi).
- la liste des sprints et de leurs nombres de jours prévus (front et back).
- la liste des ressources (développeurs), avec leur nombre de jours de disponibilité par semaine et leurs périodes de congés.

Les données d'entrée sont fournies via un fichier JSON (cf. `sample_data.json`).

Le fichier `gantt.py` contient le code qui peut être utilisé dans un notebook IPython afin de visualiser résulat sous forme de tableau et de diagramme de de Gantt.

Avec Visual Studio Code, cela peut se faire très facilement via l'extension [Jupytext](https://marketplace.visualstudio.com/items?itemName=congyiwu.vscode-jupytext) et "Open as a Jupyter notebook" depuis l'explorateur de fichiers de vscode.