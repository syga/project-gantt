import json
from io import StringIO
from dataclasses import dataclass
from datetime import date, timedelta
from typing import Generator

from workalendar.europe import France

calendar = France()


@dataclass(frozen=True)
class Sprint:
    name: str
    days_back: int
    days_front: int


@dataclass(frozen=True)
class Resource:
    name: str
    days_per_week: float
    holidays: tuple[(date, date)]

    def is_on_holiday(self, day: date) -> bool:
        return any(start <= day <= end for start, end in self.holidays)


def iter_weekdays(start_date: date) -> Generator[tuple[date], None, None]:
    assert start_date.weekday() == 0, "start_date must be a Monday"
    while True:
        yield tuple(start_date + timedelta(days=i) for i in range(5))
        start_date += timedelta(days=7)


def compute_sprint_end(
    start_date: date, days_duration: int, resources: tuple[Resource]
) -> date:
    """Computes the end date of a sprint given its start date, duration in days and
    available resources. This end date is always the last working day of a week
    (typically a Friday).
    It assumes a resource always works up to its max "days per week" on this project.
    I.e. if it's available for 3 days per week and the week has only 3 working days,
    it will still work 3 days on the project.
    """
    for weekdays in iter_weekdays(start_date):
        worked_days = [day for day in weekdays if calendar.is_working_day(day) is True]
        days_duration -= sum(
            min(
                len([d for d in worked_days if resource.is_on_holiday(d) is False]),
                resource.days_per_week,
            )
            for resource in resources
        )
        if days_duration <= 0:
            return weekdays[-1]


def load_project_data(
    file_object: StringIO,
) -> tuple[date, tuple[Resource], tuple[Resource], tuple[Sprint]]:
    start_date, resources_data, sprints_data = json.load(
        file_object, object_hook=json_date_parser
    ).values()
    return (
        start_date,
        tuple(Resource(**resource) for resource in resources_data["front"]),
        tuple(Resource(**resource) for resource in resources_data["back"]),
        tuple(Sprint(**sprint) for sprint in sprints_data),
    )


def json_date_parser(json_dict: dict) -> dict:
    for key, value in json_dict.items():
        try:
            json_dict[key] = parse_date(value)
        except (TypeError, ValueError):
            pass
    return json_dict


def parse_date(date_str: str | list[list | date]) -> date | list[list | date]:
    if isinstance(date_str, list):
        return [parse_date(d) for d in date_str]
    return date.fromisoformat(date_str)