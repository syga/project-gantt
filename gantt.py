# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     custom_cell_magics: kql
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.2
#   kernelspec:
#     display_name: venv
#     language: python
#     name: python3
# ---

# %%
from datetime import timedelta

import plotly.express as px
import pandas as pd

from planning import load_project_data, compute_sprint_end

with open("sample_data.json", "r") as f:
    project_start, fronts, backs, sprints = load_project_data(f)
    
steps = []
sprint_start = project_start
for sprint in sprints:
    sprint_end = max(
        compute_sprint_end(sprint_start, sprint.days_back, backs),
        compute_sprint_end(sprint_start, sprint.days_front, fronts),
    )
    steps.append(dict(Sprint=sprint.name, Start=sprint_start, Finish=sprint_end))
    # Sprint always end on the last working day of the week, next one starts on Monday
    sprint_start = sprint_end + timedelta(days=7 - sprint_end.weekday())

# %%
# Print the result as markdown table
df = pd.DataFrame(steps)
print(df.to_markdown())


# %%
# Print the result as a Gantt chart
fig = px.timeline(df, x_start="Start", x_end="Finish", y="Sprint")
fig.update_xaxes(dtick="M1") # monthly ticks
fig.update_yaxes(autorange="reversed")  # otherwise tasks are listed from the bottom up
fig.show()
